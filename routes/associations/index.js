const router = require("express").Router();
const Association = require("../../models/Association");

router
  .route("/") // /associations

  // GET*

  .get((req, res) => {
    // Récupérer la liste des associations depuis MongoDB
    Association.find((error, result) => {
      if (error) {
        // Envoi de l'erreur en cas de problème
        return res
          .status(500)
          .send("Erreur lors de la récupération des associations");
      } else {
        // Envoi de la liste des résultats (les associations)
        return res.send(result);
      }
    });
  })

  // POST

  .post((req, res) => {
    // Extraction des paramètres du corps de la requête
    const { body } = req;
    const {
      name,
      short_description,
      presentation,
      attached_school_id,
      address,
      phone,
      email,
      instagram,
      facebook,
      twitter,
      tiktok,
      linkedin,
      snapchat,
      messenger,
      whatsapp,
      twitch,
      discord,
      telegram,
      nb_followers,
    } = body;

    // Validation des paramètres du corps de la requête
    if (!name) return res.status(500).send("name is missing");
    if (!short_description)
      return res.status(500).send("short_description is missing");
    if (!attached_school_id)
      return res.status(500).send("attached_school_id is missing");

    const association = new Association({
      name: name,
      short_description: short_description,
      presentation: presentation,
      attached_school_id: attached_school_id,
      address: address,
      phone: phone,
      email: email,
      instagram: instagram,
      facebook: facebook,
      twitter: twitter,
      tiktok: tiktok,
      linkedin: linkedin,
      snapchat: snapchat,
      messenger: messenger,
      whatsapp: whatsapp,
      twitch: twitch,
      discord: discord,
      telegram: telegram,
    });

    association.save((error, result) => {
      if (error) return res.status(500).send(error);
      Association.find((error, result) => {
        if (error) {
          // Envoi de l'erreur en cas de problème
          return res
            .status(500)
            .send("Erreur lors de la récupération des associations");
        } else {
          // Envoi de la liste des résultats (les associations)
          return res.send(result);
        }
      });
    });
  })

  // DELETE

  .delete((req, res) => {
    // On a besoin de l'id de l'élément à supprimer
    const { body } = req;
    const { id } = body;

    if (!id) return res.status(500).send("ID is missing");

    // Récupération de l'objet à supprimer + suppression
    Association.findByIdAndDelete(id, (error, result) => {
      if (error) res.status(500).send(error);
      Association.find((error, result) => {
        if (error) {
          // Envoi de l'erreur en cas de problème
          return res
            .status(500)
            .send("Erreur lors de la récupération des associations");
        } else {
          // Envoi de la liste des résultats (les associations)
          return res.send(result);
        }
      });
    });
  })

  // UPDATE

  .patch((req, res) => {
    // On a besoin des données à modifier, on prend l'objet entier afin d'avoir toutes ses propriétés
    const {
      body: { association },
    } = req;
    // = const { id } = req.body
    // On a besoin de reconstituer un nouvel objet
    if (!association)
      return res.status(500).send("Association Object is missing");

    // On extrait l'ID
    const id = association._id;
    // On vérifie l'ID
    if (!id) return res.status(500).send("ID is missing");
    // On trouve le association et on le met à jour
    Association.findByIdAndUpdate(id, association, (error, result) => {
      if (error) return res.status(500).send(error);
      Association.find((error, result) => {
        if (error) {
          // Envoi de l'erreur en cas de problème
          return res
            .status(500)
            .send("Erreur lors de la récupération des associations");
        } else {
          // Envoi de la liste des résultats (les associations)
          return res.send(result);
        }
      });
    });
  });

router
  .route("/:id") // Correspond à /associations
  // GET (lister un seul élément)
  .get((req, res) => {
    // Récupérer la liste des associations depuis MongoDB
    Association.findById(req.params.id, (error, result) => {
      if (error) {
        // Envoi de l'erreur en cas de problème
        return res
          .status(500)
          .send("Erreur lors de la récupération de l'association");
      } else {
        // Envoi de la liste des résultats (les associations)
        return res.send(result);
      }
    });
  });

module.exports = router;
