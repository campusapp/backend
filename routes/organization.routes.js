const router = require("express").Router();
const organizationController = require("../controllers/organization.controller");

router.get("/", organizationController.getAll);
router.get("/user", organizationController.getByUserId);
router.get("/:id", organizationController.getOne);
router.delete("/:id", organizationController.delete);
router.put("/:id", organizationController.update);

router.post("/", organizationController.add);

module.exports = router;
