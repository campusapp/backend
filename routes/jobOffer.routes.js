const router = require("express").Router();
const jobOfferController = require("../controllers/jobOffer.controller");

router.get("/", jobOfferController.getAll);
router.post("/", jobOfferController.add);

router.get("/:id", jobOfferController.getOne);
router.delete("/:id", jobOfferController.delete);

module.exports = router;
