const router = require("express").Router();
const etablishmentController = require("../controllers/etablishement.controller");

router.get("/", etablishmentController.getAll);
router.post("/", etablishmentController.add);

module.exports = router;
