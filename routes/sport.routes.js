const router = require("express").Router();
const sportController = require("../controllers/sport.controller");

router.get("/", sportController.getAll);
router.get("/:id", sportController.getOne);
router.delete("/:id", sportController.delete);

router.post("/", sportController.add);

module.exports = router;
