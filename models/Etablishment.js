const mongoose = require("mongoose");
const { Schema } = mongoose;

const { isEmail } = require("validator");

const EtablishmentSchema = Schema(
  {
    name: {
      type: String,
      required: true,
      minLength: 3,
      maxLength: 55,
      unique: true,
    },
    short_description: {
      type: String,
      max: 256,
    },
    presentation: {
      type: String,
      max: 1024,
    },
    address: {
      type: String,
    },
    phone: {
      type: String,
    },
    email: {
      type: String,
      required: true,
      validate: [isEmail],
      lowercase: true,
      unique: true,
    },
    phone: {
      type: String,
    },
    picture: {
      type: String,
      default: "./uploads/profil/random-user.png",
    },
    followers: {
      type: [Number],
    },
    administrators: {
      type: [Number],
    },
    instagram: {
      type: String,
      // validate: [isUrl],
      lowercase: true,
    },
    facebook: {
      type: String,
      // validate: [isUrl],
      lowercase: true,
    },
    twitter: {
      type: String,
      // validate: [isUrl],
      lowercase: true,
    },
    tiktok: {
      type: String,
      // validate: [isUrl],
      lowercase: true,
    },
    linkedin: {
      type: String,
      // validate: [isUrl],
      lowercase: true,
    },
    snapchat: {
      type: String,
      // validate: [isUrl],
      lowercase: true,
    },
    messenger: {
      type: String,
      // validate: [isUrl],
      lowercase: true,
    },
    whatsapp: {
      type: String,
      // validate: [isUrl],
      lowercase: true,
    },
    twitch: {
      type: String,
      // validate: [isUrl],
      lowercase: true,
    },
    discord: {
      type: String,
      // validate: [isUrl],
      lowercase: true,
    },
    telegram: {
      type: String,
      // validate: [isUrl],
      lowercase: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Etablishment", EtablishmentSchema);
