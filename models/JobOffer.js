const mongoose = require("mongoose");
const { Schema } = mongoose;

const JobOfferSchema = Schema(
  {
    title: {
      type: String,
      required: true,
    },
    etablishment_id: {
      type: String,
      required: true,
    },
    contract_type: {
      type: String,
      required: true,
    },
    working_hours: {
      type: Number,
      required: true,
    },
    location: {
      type: String,
      required: true,
    },
    required_experience: {
      type: String,
      required: true,
    },
    study_level: {
      type: String,
      required: true,
    },
    salary: {
      type: String,
      required: true,
    },
    contract_dates: {
      type: String,
      required: true,
    },
    job_description: {
      type: String,
      required: true,
    },
    required_profile: {
      type: String,
      required: false,
    },
    status: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("JobOffer", JobOfferSchema);
