const mongoose = require("mongoose");
const { Schema } = mongoose;

const OrganizationSchema = Schema(
  {
    name: {
      type: String,
      required: true,
      minLength: 3,
      maxLength: 55,
      unique: true,
      trim: true,
    },
    short_name: {
      type: String,
      required: false,
    },
    created_by_user_id: {
      type: String,
      required: true,
    },
    organization_type: {
      type: String,
      required: true,
    },
    sport_id: {
      type: String,
      required: true,
    },
    presentation: {
      type: String,
      required: false,
      maxLength: 1024,
    },
    logo: {
      type: String,
      required: false,
    },
    color_primary: {
      type: String,
      required: false,
    },
    color_secondary: {
      type: String,
      required: false,
    },
    customer_shop_start: {
      type: Date,
      required: false,
    },
    customer_shop_end: {
      type: Date,
      required: false,
    },
    shop_banner: {
      type: String,
      required: false,
    },
    address: {
      type: String,
      required: false,
    },
    phone: {
      type: String,
      required: false,
    },
    email: {
      type: String,
      required: false,
    },
    instagram: {
      type: String,
      required: false,
    },
    facebook: {
      type: String,
      required: false,
    },
    twitter: {
      type: String,
      required: false,
    },
    tiktok: {
      type: String,
      required: false,
    },
    linkedin: {
      type: String,
      required: false,
    },
    snapchat: {
      type: String,
      required: false,
    },
    messenger: {
      type: String,
      required: false,
    },
    whatsapp: {
      type: String,
      required: false,
    },
    twitch: {
      type: String,
      required: false,
    },
    discord: {
      type: String,
      required: false,
    },
    telegram: {
      type: String,
      required: false,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Organization", OrganizationSchema);
