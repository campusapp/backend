const User = require("../models/User");
const ObjectId = require("mongoose").Types.ObjectId;

module.exports.getAll = async (req, res) => {
  User.find()
    .select("-password")
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(500).send(error));
};

module.exports.getOne = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  User.findById(req.params.id)
    .select("-password")
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(500).send(error));
};

module.exports.update = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  try {
    User.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          bio: req.body.bio,
        },
      },
      { new: true, upsert: true, setDefaultsOnInsert: true },
      (error, result) => {
        if (!error) {
          return res.status(201).send(result);
        } else {
          return res.status(500).send(error);
        }
      }
    ).select("-password");
  } catch (error) {
    return res.status(500).send(error);
  }
};

module.exports.delete = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  try {
    await User.remove({ _id: req.params.id }).exec();
    return res
      .status(200)
      .send("Suppression de l'utilisateur: " + req.params.id);
  } catch (error) {
    return res.status(500).send(error);
  }
};
