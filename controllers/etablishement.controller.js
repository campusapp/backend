const Etablishment = require("../models/Etablishment");
const ObjectId = require("mongoose").Types.ObjectId;

module.exports.getAll = async (req, res) => {
  Etablishment.find()
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(500).send(error));
};

module.exports.getOne = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  Etablishment.findById(req.params.id)
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(500).send(error));
};

module.exports.add = async (req, res) => {
  const { name, short_description, presentation, address, phone, email } =
    req.body;

  if (!name) return res.status(500).send("name is missing");
  if (!short_description)
    return res.status(500).send("short_description is missing");

  try {
    const etablishment = await Etablishment.create({
      name,
      short_description,
      presentation,
      address,
      phone,
      email,
    });
    return res.status(201).send({ etablishment: etablishment._id });
  } catch (error) {
    return res.status(500).send(error);
  }
};

module.exports.update = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  try {
    Etablishment.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          name: req.body.name,
        },
      },
      { new: true, upsert: true, setDefaultsOnInsert: true },
      (error, result) => {
        if (!error) {
          return res.status(201).send(result);
        } else {
          return res.status(500).send(error);
        }
      }
    );
  } catch (error) {
    return res.status(500).send(error);
  }
};

module.exports.delete = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  try {
    await Etablishment.remove({ _id: req.params.id }).exec();
    return res
      .status(200)
      .send("Suppression de l'établissement: " + req.params.id);
  } catch (error) {
    return res.status(500).send(error);
  }
};
