const Sport = require("../models/sport.model.js");
const ObjectId = require("mongoose").Types.ObjectId;

module.exports.getAll = async (req, res) => {
  Sport.find()
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(500).send(error));
};

module.exports.getOne = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  Sport.findById(req.params.id)
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(500).send(error));
};

module.exports.add = async (req, res) => {
  const { name, slug, image } = req.body;

  if (!name) return res.status(500).send("name is missing");
  if (!slug) return res.status(500).send("slug is missing");
  if (!image) return res.status(500).send("image is missing");

  try {
    const sport = await Sport.create({
      name,
      slug,
      image,
    });
    return res.status(201).send({ sport: sport._id });
  } catch (error) {
    return res.status(500).send(error);
  }
};

module.exports.update = async (req, res) => {
  const { name, slug, image } = req.body;

  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  try {
    Sport.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          name,
          slug,
          image,
        },
      },
      { new: true, upsert: true, setDefaultsOnInsert: true },
      (error, result) => {
        if (!error) {
          return res.status(201).send(result);
        } else {
          return res.status(500).send(error);
        }
      }
    );
  } catch (error) {
    return res.status(500).send(error);
  }
};

module.exports.delete = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  try {
    await Sport.remove({ _id: req.params.id }).exec();
    return res.status(200).send("Suppression du sport: " + req.params.id);
  } catch (error) {
    return res.status(500).send(error);
  }
};
