const JobOffer = require("../models/JobOffer");
const ObjectId = require("mongoose").Types.ObjectId;

module.exports.getAll = async (req, res) => {
  JobOffer.find()
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(500).send(error));
};

module.exports.getOne = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  JobOffer.findById(req.params.id)
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(500).send(error));
};

module.exports.add = async (req, res) => {
  const {
    title,
    etablishment_id,
    contract_type,
    working_hours,
    location,
    required_experience,
    study_level,
    salary,
    contract_dates,
    job_description,
    required_profile,
    status,
  } = req.body;

  if (!title) return res.status(500).send("title is missing");
  if (!etablishment_id)
    return res.status(500).send("etablishment_id is missing");
  if (!contract_type) return res.status(500).send("contract_type is missing");
  if (!working_hours) return res.status(500).send("working_hours is missing");
  if (!location) return res.status(500).send("location is missing");
  if (!required_experience)
    return res.status(500).send("required_experience is missing");
  if (!study_level) return res.status(500).send("study_level is missing");
  if (!salary) return res.status(500).send("salary is missing");
  if (!contract_dates) return res.status(500).send("contract_dates is missing");
  if (!job_description)
    return res.status(500).send("job_description is missing");
  if (!required_profile)
    return res.status(500).send("required_profile is missing");
  if (!status) return res.status(500).send("status is missing");

  try {
    const job_offer = await JobOffer.create({
      title,
      etablishment_id,
      contract_type,
      working_hours,
      location,
      required_experience,
      study_level,
      salary,
      contract_dates,
      job_description,
      required_profile,
      status,
    });
    return res.status(201).send({ job_offer: job_offer._id });
  } catch (error) {
    return res.status(500).send(error);
  }
};

module.exports.update = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  try {
    JobOffer.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          name: req.body.name,
        },
      },
      { new: true, upsert: true, setDefaultsOnInsert: true },
      (error, result) => {
        if (!error) {
          return res.status(201).send(result);
        } else {
          return res.status(500).send(error);
        }
      }
    );
  } catch (error) {
    return res.status(500).send(error);
  }
};

module.exports.delete = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  try {
    await JobOffer.remove({ _id: req.params.id }).exec();
    return res
      .status(200)
      .send("Suppression de l'offre d'emploi: " + req.params.id);
  } catch (error) {
    return res.status(500).send(error);
  }
};
