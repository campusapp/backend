// Controller pour l'authentification

const User = require("../models/User");
const jwt = require("jsonwebtoken");
const { signUpErrors, signInErrors } = require("../utils/errors.utils");

// Temps du token

const maxAge = 30 * 24 * 60 * 60 * 1000;
const createToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET_KEY, {
    expiresIn: maxAge,
  });
};

// Inscription

module.exports.signUp = async (req, res) => {
  const { username, email, password, role } = req.body;

  if (!username) return res.status(400).send("Error: username not found");
  if (!email) return res.status(400).send("Error: email not found");
  if (!password) return res.status(400).send("Error: password not found");

  try {
    const user = await User.create({ username, email, password, role });
    return res.status(201).send({ user: user._id });
  } catch (error) {
    const errors = signUpErrors(error);
    return res.status(500).send(errors);
  }
};

// Connecion

module.exports.signIn = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.login(email, password);
    const token = createToken(user._id);
    res.cookie("jwt", token, { httpOnly: true, maxAge });
    res.status(200).json({ user: user._id });
  } catch (err) {
    const errors = signInErrors(err);
    res.status(200).json({ errors });
  }
};

// Desinscription

module.exports.logout = async (req, res) => {
  try {
    res.cookie("jwt", "", { maxAge: 1 });
    return res.status(200).send("Utilisateur déconnecté");
  } catch (error) {
    return res.status(500).send(error);
  }
};
