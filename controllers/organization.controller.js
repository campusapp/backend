const Organization = require("../models/organization.model.js");
const ObjectId = require("mongoose").Types.ObjectId;

module.exports.getAll = async (req, res) => {
  Organization.find()
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(500).send(error));
};

module.exports.getOne = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  Organization.findById(req.params.id)
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(500).send(error));
};

module.exports.getByUserId = async (req, res) => {
  const { userId } = req.body;

  if (!userId) return res.status(400).send("Error: userId not found");

  Organization.find({ created_by_user_id: userId })
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(500).send(error));
};

module.exports.add = async (req, res) => {
  const {
    name,
    short_name,
    organization_type,
    sport_id,
    presentation,
    logo,
    color_primary,
    color_secondary,
    customer_shop_start,
    customer_shop_end,
    shop_banner,
    address,
    phone,
    email,
    instagram,
    facebook,
    twitter,
    tiktok,
    linkedin,
    snapchat,
    messenger,
    whatsapp,
    twitch,
    discord,
    telegram,
    created_by_user_id,
  } = req.body;

  if (!name) return res.status(500).send("name is missing");
  if (!organization_type)
    return res.status(500).send("organization_type is missing");
  if (!sport_id) return res.status(500).send("sport_id is missing");
  if (!created_by_user_id)
    return res.status(500).send("created_by_user_id is missing");

  try {
    const organization = await Organization.create({
      name,
      short_name,
      organization_type,
      sport_id,
      presentation,
      logo,
      color_primary,
      color_secondary,
      customer_shop_start,
      customer_shop_end,
      shop_banner,
      address,
      phone,
      email,
      instagram,
      facebook,
      twitter,
      tiktok,
      linkedin,
      snapchat,
      messenger,
      whatsapp,
      twitch,
      discord,
      telegram,
      created_by_user_id,
    });
    return res.status(201).send({ organization: organization._id });
  } catch (error) {
    return res.status(500).send(error);
  }
};

module.exports.update = async (req, res) => {
  const {
    name,
    short_name,
    organization_type,
    sport_id,
    presentation,
    logo,
    color_primary,
    color_secondary,
    customer_shop_start,
    customer_shop_end,
    shop_banner,
    address,
    phone,
    email,
    instagram,
    facebook,
    twitter,
    tiktok,
    linkedin,
    snapchat,
    messenger,
    whatsapp,
    twitch,
    discord,
    telegram,
    created_by_user_id,
  } = req.body;

  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  try {
    Organization.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          name,
          short_name,
          organization_type,
          sport_id,
          presentation,
          logo,
          color_primary,
          color_secondary,
          customer_shop_start,
          customer_shop_end,
          shop_banner,
          address,
          phone,
          email,
          instagram,
          facebook,
          twitter,
          tiktok,
          linkedin,
          snapchat,
          messenger,
          whatsapp,
          twitch,
          discord,
          telegram,
          created_by_user_id,
        },
      },
      { new: true, upsert: true, setDefaultsOnInsert: true },
      (error, result) => {
        if (!error) {
          return res.status(201).send(result);
        } else {
          return res.status(500).send(error);
        }
      }
    );
  } catch (error) {
    return res.status(500).send(error);
  }
};

module.exports.delete = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send("Error: ID unknown (" + req.params.id + ")");

  try {
    await Organization.remove({ _id: req.params.id }).exec();
    return res
      .status(200)
      .send("Suppression de l'organization: " + req.params.id);
  } catch (error) {
    return res.status(500).send(error);
  }
};
