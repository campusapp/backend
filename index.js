require("dotenv").config();

const express = require("express");
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const crypto = require("crypto");
const fs = require("fs");

const multer = require("multer");
const sftpStorage = require("multer-sftp");

// Setup SFTP

var storage = sftpStorage({
  sftp: {
    host: "109.234.162.236",
    username: "gatd2393",
    port: 22,
    privateKey: fs.readFileSync("./id_rsa"),
    passphrase: "lOTjuCv1wlkt",
  },
  destination: function (req, file, cb) {
    cb(null, "one-sport-datas/uploads");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + ".jpg");
  },
});

const upload = multer({ storage: storage });

const app = express();

const userRoutes = require("./routes/user.routes");
const etablishmentRoutes = require("./routes/etablishment.routes");
const sportRoutes = require("./routes/sport.routes");
const jobOfferRoutes = require("./routes/jobOffer.routes");
const organizationRoutes = require("./routes/organization.routes");

const corsOptions = {
  origin: process.env.CLIENT_URL,
  credentials: true,
  allowedHeaders: ["sessionId", "Content-Type"],
  exposedHeaders: ["sessionId"],
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  preflightContinue: false,
};
app.use(cors(corsOptions));

const { checkUser, requireAuth } = require("./middlewares/auth.middleware");

// Initialisation de Express pour utiliser le body des requêtes au format UrlEncoded et JSON
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(express.static("public"));
app.use("/uploads", express.static("uploads"));

//décoder les cookies
app.use(cookieParser());

const router = express.Router();

// On définit le port d'écoute
const port = process.env.PORT;

// Chaîne de connexion à la base de données MongoDB
const mongoDbConnectionString = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_NAME}?retryWrites=true&w=majority`;

// Lancement de la connexion à la base de données avec les paramètres précédents
mongoose.connect(mongoDbConnectionString, null, (error) => {
  if (error) throw Error;
});

//JWT

app.get("*", checkUser);
// app.get("/api/user", checkUser);
app.get("/api/jwtid", requireAuth, (req, res) => {
  return res.status(200).send(res.locals.user._id);
});

// Récupération de la connexion
const db = mongoose.connection;
// Listener de connexion pour valider la connexion
db.once("open", () => {
  console.info("Connexion à la base : OK");
});

// ROUTES

app.get("/api", (req, res) => {
  res.send("Server running");
});

// Test upload d'image

app.post("/api/sport/upload", upload.single("file"), (req, res, next) => {
  return res.send(req.file);
});

app.use("/api/user", userRoutes);
app.use("/api/etablishment", etablishmentRoutes);
app.use("/api/job-offer", jobOfferRoutes);
app.use("/api/sport", sportRoutes);
app.use("/api/organization", organizationRoutes);

app.use("/api/associations", require("./routes/associations"));

// Lancement du server
app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`);
});
