const jwt = require("jsonwebtoken");
const User = require("../models/User");

module.exports.checkUser = (req, res, next) => {
  const token = req.cookies.jwt;
  if (token) {
    jwt.verify(
      token,
      process.env.JWT_SECRET_KEY,
      async (error, decodedToken) => {
        if (error) {
          res.status(401).send("Unauthorized: Invalid token");
          res.cookie("jwt", "", { maxAge: 1 });
          next();
        } else {
          let user = await User.findById(decodedToken.id);
          res.locals.user = user;
          next();
        }
      }
    );
  } else {
    res.status(401).send("Unauthorized: No token provided");
    res.locals.user = null;
    next();
  }
};

module.exports.requireAuth = (req, res, next) => {
  const token = req.cookies.jwt;

  if (token) {
    jwt.verify(
      token,
      process.env.JWT_SECRET_KEY,
      async (error, decodedToken) => {
        if (error) {
          res.status(500).send(error);
        } else {
          next();
        }
      }
    );
  } else {
    res.status(401).send("Unauthorized: No token provided");
  }
};
