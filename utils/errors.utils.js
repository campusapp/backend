module.exports.signUpErrors = (error) => {
  let errors = { username: "", email: "", password: "" };

  if (error.message.includes("username")) errors.username = "Pseudo incorrect";
  if (error.message.includes("email"))
    errors.email = "Adresse e-mail incorrect";
  if (error.message.includes("password"))
    errors.password = "Mot de passe incorrect";

  if (
    error.code === 11000 &&
    Object.keys(error.keyValue)[0].includes("username")
  )
    errors.username = "Ce pseudo est déjà utilisé";

  if (error.code === 11000 && Object.keys(error.keyValue)[0].includes("email"))
    errors.email = "Cette adresse e-mail est déjà utilisée";

  return errors;
};

module.exports.signInErrors = (error) => {
  let errors = { error: "L'adresse e-mail ou le mot de passe est incorrect" };

  return errors;
};
